import logging

import pendulum
from kombu import Connection, Producer, Exchange, Queue
from kombu.mixins import ConsumerProducerMixin


def get_module_logger(mod_name):
    """
    To use this, do logger = get_module_logger(__name__)
    """
    logger = logging.getLogger(mod_name)
    handler = logging.StreamHandler()
    formatter = logging.Formatter(
        '%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    return logger


logger = get_module_logger(__name__)


tasks_exchange = Exchange('tasks', 'fanout', durable=True)
py_request_queue = Queue('py_request', exchange=tasks_exchange, routing_key='msg')
rb_request_queue = Queue('rb_request', exchange=tasks_exchange, routing_key='msg')
result_exchange = Exchange('result', 'direct', durable=True)
response_queue = Queue('response', exchange=result_exchange, routing_key='reply_to')


class Worker(ConsumerProducerMixin):

    def __init__(self, connection):
        self.connection = connection

    def get_consumers(self, Consumer, channel):
        return [Consumer(
            queues=[py_request_queue],
            on_message=self.on_message,
            accept={'application/json'},
            prefetch_count=1,
        )]

    def on_message(self, message):
        current_time = pendulum.now('Europe/Amsterdam')
        print(f" [x]-1 Received {current_time} {message.payload['date-time']}")
        print(f" [x]-1 Received {current_time.diff(pendulum.parse(message.payload['date-time']))._delta}")
        # print(f" [x]-1 Received {len(message.payload['content'])}")
        message.payload['response'] = f"{current_time}"

        self.producer.publish(
            message.payload,
            exchange=result_exchange, routing_key='reply_to',
            # exchange=tasks_exchange, routing_key='msg',
            serializer='json',
            retry=True,
        )
        print("  message produced !!!!!!!!!!")
        message.ack()


def start_worker(broker_url):
    connection = Connection(broker_url, connect_timeout=10)
    print(' [x]-1 Awaiting requests')
    worker = Worker(connection)
    worker.run()


if __name__ == '__main__':
    try:
        start_worker('amqp://guest:guest@rabbitmq:5672//')
    except KeyboardInterrupt:
        pass
