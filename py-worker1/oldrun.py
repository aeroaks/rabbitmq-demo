from flask import Flask
from flask import request
from flask import jsonify
from kombu import Connection, Exchange, Queue
import json
import logging
import pendulum

def get_module_logger(mod_name):
    """
    To use this, do logger = get_module_logger(__name__)
    """
    logger = logging.getLogger(mod_name)
    handler = logging.StreamHandler()
    formatter = logging.Formatter(
        '%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    return logger


logger = get_module_logger(__name__)


service_exchange = Exchange('service', 'direct', durable=True)
request_queue = Queue('request', exchange=service_exchange, routing_key='msg')


def process_media(body, message):
    # print(f" [x] Received {json.loads(body['content']).keys()}, {len(json.loads(body['content'])['values'])}")
    current_time = pendulum.now('Europe/Amsterdam')
    print(f" [x]-1 Received {current_time} {body['date-time']}")
    print(f" [x]-1 Received {current_time.diff(pendulum.parse(body['date-time']))._delta}")
    print(f" [x]-1 Received {len(body['content'])}")
    message.ack()


# connections
with Connection('amqp://guest:guest@rabbitmq:5672//', connect_timeout=10) as conn:
    # the declare above, makes sure the video queue is declared
    # so that the messages can be delivered.
    # It's a best practice in Kombu to have both publishers and
    # consumers declare the queue. You can also declare the
    # queue manually using:
    #     video_queue(conn).declare()

    # consume
    with conn.Consumer(request_queue, callbacks=[process_media]) as consumer:
        # Process messages and handle events on all channels
        while True:
            conn.drain_events()
