import logging

import pendulum
from kombu import Connection, Producer, Consumer, Queue, Exchange, uuid


def get_module_logger(mod_name):
    """
    To use this, do logger = get_module_logger(__name__)
    """
    logger = logging.getLogger(mod_name)
    handler = logging.StreamHandler()
    formatter = logging.Formatter(
        '%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    return logger


logger = get_module_logger(__name__)


tasks_exchange = Exchange('tasks', 'fanout', durable=True)
py_request_queue = Queue('py_request', exchange=tasks_exchange, routing_key='msg')
rb_request_queue = Queue('rb_request', exchange=tasks_exchange, routing_key='msg')
result_exchange = Exchange('result', 'direct', durable=True)
response_queue = Queue('response', exchange=result_exchange, routing_key='reply_to')

def process_message(body, message):
    print(f"--------------------------------The body is {body}")
    message.ack()


def emit_user_profile_update(user_id, new_data):

    with Connection('amqp://guest:guest@rabbitmq:5672//', connect_timeout=10) as conn:
        # # produce
        # producer = conn.Producer(serializer='json')
        # with open("services/value.json") as f:
        #     # producer.publish({'content': {'full_name': new_data}},
        #     print(f" [x] Producer sending file")
        #     logger.debug(f" {pendulum.now('Europe/Amsterdam')} [x] Producer sending file")
        #     file_contents = f.read()
        #     for i in range(1):
        #         producer.publish(
        #             {'content': file_contents,
        #              'date-time': f"{pendulum.now('Europe/Amsterdam')}"},
        #             exchange=tasks_exchange, routing_key='msg',
        #             declare=[request_queue]
        #         )

        producer = conn.Producer(serializer='json')
        # producer.publish({'content': {'full_name': new_data}},
        print(f" [x] Producer sending msg")
        logger.debug(f" {pendulum.now('Europe/Amsterdam')} [x] Producer sending msg")
        producer.publish(
            {'date-time': f"{pendulum.now('Europe/Amsterdam')}"},
            exchange=tasks_exchange, routing_key='msg',
            declare=[py_request_queue, rb_request_queue]
        )

        with Consumer(conn, response_queue, callbacks=[process_message], accept=['json', 'application/octet-stream']):
            conn.drain_events(timeout=5)

    return 'Sent: '


# class MessageClient(object):

#     def __init__(self, connection):
#         self.connection = connection
#         self.callback_queue = Queue(uuid(), exclusive=True, auto_delete=True)

#     def on_response(self, message):
#         if message.properties['correlation_id'] == self.correlation_id:
#             self.response = message.payload['result']

#     def call(self):
#         self.response = None
#         self.correlation_id = uuid()
#         with Producer(self.connection) as producer:
#             # with open("services/value.json") as f:
#             #     file_contents = f.read()
#             print(f" [x] Producer sending file")
#             logger.debug(f" {pendulum.now('Europe/Amsterdam')} [x] Producer sending file")
#             producer.publish(
#                 {'date-time': f"{pendulum.now('Europe/Amsterdam')}"},
#                 exchange=tasks_exchange,
#                 routing_key='msg',
#                 declare=[self.callback_queue],
#                 reply_to=self.callback_queue.name,
#                 correlation_id=self.correlation_id,
#             )
#         with Consumer(self.connection,
#                       on_message=self.on_response,
#                       queues=[self.callback_queue], no_ack=True):
#             while self.response is None:
#                 self.connection.drain_events()
#         print(f" [x] {self.response}")
#         return self.response


# def emit_user_profile_update(user_id, new_data):

#     connection = Connection('amqp://guest:guest@rabbitmq:5672//', connect_timeout=10)
#     msg_client = MessageClient(connection)
#     print(' [x] Sending file ...................................!!!!!')
#     response = msg_client.call()
#     print(f' [.] Got {response}')