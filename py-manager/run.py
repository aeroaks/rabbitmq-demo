#!/usr/local/bin python
# -*- coding: utf-8 -*-
from flask import Flask
from flask import request
from flask import jsonify
from services.event_handler import emit_user_profile_update
import logging

app = Flask(__name__)


def get_module_logger(mod_name):
    """
    To use this, do logger = get_module_logger(__name__)
    """
    logger = logging.getLogger(mod_name)
    handler = logging.StreamHandler()
    formatter = logging.Formatter(
        '%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    return logger


logger = get_module_logger(__name__)


@app.route('/users/<int:user_id>', methods=['POST'])
def update(user_id):
    new_name = request.form['full_name']

    # Update the user in the datastore using a local transaction...
    # print("test")
    out = emit_user_profile_update(user_id, {'full_name': new_name})

    return jsonify({'full_name': out + new_name}), 201


if __name__ == '__main__':
    app.run(port=8760, debug=True, use_reloader=False, threaded=False, host="0.0.0.0")
