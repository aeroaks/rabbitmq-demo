#!/usr/bin/env ruby
require 'bunny'

connection = Bunny.new(:host => "rabbitmq", :port => 5672, :user => "guest", :password => "guest")
connection.start

channel = connection.create_channel
tasks = channel.fanout('tasks', :durable => true)
py_request = channel.queue('py_request', :durable => true).bind(tasks, :routing_key => "msg")
rb_request = channel.queue('rb_request', :durable => true).bind(tasks, :routing_key => "msg")
result = channel.direct('result', :durable => true)
response = channel.queue('response', :durable => true).bind(result, :routing_key => "reply_to")

STDERR.puts "IT HAS BEGUN"

begin
  STDERR.puts ' [*] Waiting for messages. To exit press CTRL+C'
  # block: true is only used to keep the main thread
  # alive. Please avoid using it in real world applications.
  rb_request.subscribe(block: true) do |_delivery_info, _properties, body|
    STDERR.puts " [x] Received #{body}"
    response.publish('Hello World! ' << "#{body}", routing_key: 'reply_to')
    STDERR.puts " [x] Sent 'Hello World!'"
  end
rescue Interrupt => _
  STDERR.puts "for fuck's sake #{_.message}"

  connection.close

  exit(0)
end